package com.sin.codes;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class register
 */
@WebServlet("/register")
public class register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uname=request.getParameter("user");
		String pword=request.getParameter("pass");
		String desig=request.getParameter("desig");
		int id=Integer.parseInt(request.getParameter("id"));
		String email=request.getParameter("email");
		registerService rs=new registerService();
		boolean reg=rs.registerUser(uname,pword,desig,id,email);
		if(reg)
		{
			response.sendRedirect("loginS.html");
		}
		else
		{
			response.sendRedirect("login.html");
		}
	}

}

