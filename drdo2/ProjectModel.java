package com.sin.codes.dto;

public class ProjectModel {
		 private int projectnum;
		 private String projecttitle;
		 private String category;
		 private String manager;
		 private String leader;
		 private String description;
		 private String startDate;
		 private String enddate;
		 private String status;
		 public int getProjectnum() {
			return projectnum;
		}
		public void setProjectnum(int projectnum) {
			this.projectnum = projectnum;
		}
		public String getProjecttitle() {
			return projecttitle;
		}
		public void setProjecttitle(String projecttitle) {
			this.projecttitle = projecttitle;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		public String getManager() {
			return manager;
		}
		public void setManager(String manager) {
			this.manager = manager;
		}
		public String getLeader() {
			return leader;
		}
		public void setLeader(String leader) {
			this.leader = leader;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getStartDate() {
			return startDate;
		}
		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}
		public String getEnddate() {
			return enddate;
		}
		public void setEnddate(String enddate) {
			this.enddate = enddate;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
 }

