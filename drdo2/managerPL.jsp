<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.sin.codes.dto.ProjectModel,com.sin.codes.dto.UserModel"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>projects</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>
		<div id="main" class="wrapper style1">
		<div class="container">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo"><a href="profile-admin.html">DRDO</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="profile-admin.html">Home</a></li>
							<li>
								<a href="#">Projects</a>

							</li>

							<li><a href="add-new-project.html" class="button specialg">Create new project</a></li>

							<li><a href="login.html" class="button special">Sign out</a></li>
						</ul>
					</nav>
				</header>

				<div class="table-wrapper">
						<%
						UserModel user=(UserModel)request.getSession().getAttribute("users");
						int i=0;
							if(user.getProjectnum()==0)
							{%>
							<h2>No projects assigned</h2>
							
							<%}
							else 
							{
								ProjectModel[] pmm=(ProjectModel[])request.getSession().getValue("projects");
								System.out.println(pmm[0].getCategory());
								int len=pmm.length;
							%><%="<h2>"+pmm[0].getStatus()+"</h2>"%> <div class="table-wrapper">
							<table class="alt">
							<thead>

								<tr>
									<th>Project Name</th>
									<th>Description</th>
									<th>manager</th>
									<th>leader</th>
									<th>start-Date</th>
									<th>PDC</th>
									<th>status</th>
								</tr>

							</thead>
							<tbody>
								<% for(i=0;i<len;i++)
								{%>
									
									<%="<tr><td>"+pmm[i].getProjecttitle()+"</td>"%>
									<%="<td>"+pmm[i].getDescription()+"</td>" %>
									<%="<td>"+pmm[i].getManager()+"</td>"%>
									<%="<td>"+pmm[i].getLeader()+"</td>"%>
									<%="<td>"+pmm[i].getStartDate()+"</td>"%>
									<%="<td>"+pmm[i].getEnddate()+"</td>" %>
									<%="<td>"+pmm[i].getStatus()+"<div class=\"3u 6u$(medium) 12u$(xsmall)\"><ul class=\"actions vertical small\"<li><a href=\"modifyp.jsp\" class=\"button special small\">modify</a></li></ul></div></td></tr>"%>
								<%
								} 
							} %>
						</tbody>
						

					</table>
				</div>
			</div>
		</div>

		</body>
  </html>
