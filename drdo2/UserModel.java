package com.sin.codes.dto;

public class UserModel {
	private int idno;
	private String uname;
	private String designation;
	private String emailid;
	private int projectnum;
	private String password;
	public int getIdno() {
		return idno;
	}
	public void setIdno(int idno) {
		this.idno = idno;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public int getProjectnum() {
		return projectnum;
	}
	public void setProjectnum(int projectnum) {
		this.projectnum = projectnum;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}

