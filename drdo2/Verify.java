package com.sin.codes;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sin.codes.dto.ProjectModel;
import com.sin.codes.dto.UserModel;

/**
 * Servlet implementation class Verify
 */
@WebServlet("/Verify")
public class Verify extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uname=request.getParameter("user");
		String pword=request.getParameter("pass");
		/*
		String desig=request.getParameter("desig");
		String id=request.getParameter("id");
		String email=request.getParameter("email");*/
		VerifyService vf=new VerifyService();
		boolean verify=vf.verifyUser(uname, pword);
		if(verify)
		{
			System.out.println("success");
			VerifyService vs=new VerifyService();
			UserModel um=vs.getUser(uname);
			System.out.println(um.getUname());
			ProjectModel[] pm=new ProjectModel[500];
			pm=vs.getProject(uname);
			request.getSession().setAttribute("user",um);
			request.getSession().setAttribute("project",pm);
			if(um.getDesignation().equals("Trainee"))
			response.sendRedirect("traineep.jsp");
			else if(um.getDesignation().equals("Manager"))
				response.sendRedirect("Managerp.jsp");
			return;
		}
		else
		{
			System.out.println("fail");
			response.sendRedirect("login.html");
			return;
		}
	}

}

